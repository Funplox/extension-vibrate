package;

#if cpp
import cpp.Lib;
#elseif neko
import neko.Lib;
#end

#if (android && openfl)
import openfl.utils.JNI;
#end


class VibrateExtension {
	
	
	public static function vibrate (time:Int):Int {
		
		#if (android && openfl)
		
		var resultJNI = vibrate_method_jni(time);
		var resultNative = vibrate_method(time);
		
		if (resultJNI != resultNative)
		{
			
			throw "Fuzzy math!";
			
		}
		
		return resultNative;
		
		#else
		
		return vibrate_method(time);
		
		#end
		
	}
	
	
	private static var vibrate_method = Lib.load ("vibrateextension", "vibrate_method", 1);
	
	#if (android && openfl)
	private static var vibrate_method_jni = JNI.createStaticMethod ("org.haxe.extension.VibrateExtension", "vibrate", "(I)I");
	#end
	
	
}